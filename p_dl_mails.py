#!/usr/local/bin/python3.4
import zipfile
from decimal import *
from pprint import *
import sys
import imaplib
import email
import os
import logging
logging.basicConfig(filename='/var/reconwise/logs/dl_mails.log',level=logging.DEBUG)


f0 = folder_stmts = '/Users/apandey/Documents/TamTech/NE - Daily Statements'
f1 = folder_recon_ouputs = '/Users/apandey/Documents/TamTech/NE Reconcilers - Output'
# f0 = folder_stmts = '/Users/apandey/Documents/TamTech/ne_stmt'
# f1 = folder_recon_ouputs = '/Users/apandey/Documents/TamTech/ne_output'

class Account:
    def __init__(self, last_uid):
        self.last_uid = str(last_uid)

    def login(self):
        logging.debug("Email_Service::_check_mail:: Time to check mail")
        self._mailbox = imaplib.IMAP4_SSL('imap.gmail.com')

        try:
            rv, data = self._mailbox.login('abhishek.pandey@tampinestechnology.com', 'Tsuguri0')
            logging.debug("Worked for abhishek")
            logging.debug("EMAIL DATA:%s ", data)
        except imaplib.IMAP4.error:
            logging.debug("LOGIN FAILED for abhishek")
            return False
        return True
    def logout(self):
        self._mailbox.logout()

    def run(self):
        if self.login():
            self.read_mails()
            self.logout()

    def read_mails(self):
        rv, data = self._mailbox.select('GH')
        command = "UID {}:*".format(self.last_uid)
        # command = "(SEEN) UID {}:*".format(self.last_uid)
        logging.debug('command={}'.format(command))
        result, mail_numbers = self._mailbox.uid('search', None, command)
        messages = mail_numbers[0].split()
        logging.debug('messages={}'.format(messages))
        # yield mails
        
        for message_uid in messages:
            message_uid_i = int(message_uid)
            # SEARCH command *always* returns at least the most
            # recent message, even if it has already been synced
            result, mail_data_raw = self._mailbox.uid('fetch', message_uid, '(RFC822)')
            # yield raw mail body
            if mail_data_raw[0] is not None:
                self.process_mail(message_uid_i, mail_data_raw)

    def process_mail(self, num, mail_data):
        header_decoder = lambda x: email.header.decode_header(mail[x])[0][0]
        mail = email.message_from_string(mail_data[0][1].decode("utf-8"))
        logging.debug("\n\n %s", "#_" * 90)
        subject = header_decoder('Subject')
        sbu = subject.upper()
        all_subs = [x[0].upper() for x in items]
        found = False
        try:
            found = next((s for s in all_subs if s in sbu), None)
        except Exception as e:
            print('Exception: e: {}'.format(str(e)))

        # if sbu not in all_subs:
        if not found:
            logging.debug('Subject not matching. Ignoring {}'.format(subject))
            return

        if mail.get_content_maintype() != 'multipart':
            return 
        folder = next((d[1] for d in items if d[0].upper() in sbu), '')
        
        if not folder:
            return

        mail_from = header_decoder('From').upper()
        mail_date = header_decoder('Date').upper()
        logging.debug("UID={}; Subject={}\n\tFrom={}\n".format(num, subject, mail_from))

        logging.debug('Message:: {}'.format(subject))
        logging.debug('-' * 30 + '\n\t')


        for part in mail.walk():

                        # multipart are just containers, so we skip them
            if part.get_content_maintype() == 'multipart':
                                continue

            # Confirm that there's an attachment ?
            if part.get('Content-Disposition') is None:
                                continue

            filename = part.get_filename()

            att_path = os.path.join(folder, filename)
            if os.path.exists(att_path.replace('.zip', '.xls')):
                continue
            logging.debug("Path on OS=%s", att_path)

            # finally write the stuff
            fp = open(att_path, 'wb')
            email_data_file = part.get_payload(decode=True)
            fp.write(email_data_file)
            fp.close()
            

            if att_path.endswith('.zip'):
                zip_ref = zipfile.ZipFile(att_path, 'r')
                zip_ref.extractall(folder, pwd=b'crossst')
                zip_ref.close()

                os.remove(att_path)
            logging.debug("FILE SAVED: %s", att_path)

items = [
        ('Daily Statements', f0),
        ('Newedge reconciler report', f1)
        ]

acc = Account(sys.argv[1] if len(sys.argv) > 1 else 3375)
acc.run()
