import traceback
import sys
from math import fabs
from itertools import chain
from functools import reduce
import operator
#from django.http import HttpResponse, HttpResponseBadRequest
#from django.shortcuts import render, get_object_or_404

#from django.db.models.query_utils import Q
#from django.conf import settings
# from django.views.decorators.csrf import csrf_exempt
from slugify import slugify
import xlrd
import uuid
from simplejson import dumps
import re

import os


import pprint
from django.db.models import Sum, Avg, Count
import django, os, sys
from django.db.models import F, FloatField
from django.db.models.query import QuerySet
from collections import defaultdict
import pprint, datetime
from itertools import chain, groupby


rw_path = '/Users/apandey/code/tilde/reconwisev2/rw'
os.chdir(rw_path)
sys.path.append(rw_path)
print('path={}'.format(os.getcwd()))


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "rw.settings")
os.chdir(rw_path)
django.setup()

from confo.views import *
from confo.models import *
from broker_parser.models import *
from rw.utils import TZ_CHOICES, A_DAY, EXPIRY


SEP = ','
PIPE = '|'
latest_id =  Confo.objects.filter(filename1__exact='',filename2__exact='').order_by('-date').values_list('id')[0][0]
confo_id=str(latest_id)
# confo_id='199'
c = Confo.objects.get(pk=confo_id)
mnth = lambda i: "{0:02d}".format(EXPIRY.find(i.upper()))
