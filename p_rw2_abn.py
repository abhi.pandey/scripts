from base_rw1 import *


def _upload_ftp_files(pattern='*.csv', folder='/var/reconwise/ftp'):
    files = find_dirs_and_files(
        pattern,
        folder,
        False, True, True
        )
    
    
    # files = [files[2]]
    s = get_services()
    f = s[1]

    for file in files:
        dt = utilities.filename_to_date(file)
        if dt.year == 2015 and dt.month == 7:
            print('file={}; date={}'.format(file, dt))
            # f._pass_to_client_service(file)
            # f._upload_file_to_v2(file)
            print('FILE UPLOADED')
            # break

    print('PROCESS COMPLETED')

if __name__ == "__main__":
    _upload_ftp_files('CLIF-6989-V4.*.zip', '/var/reconwise/abn')

