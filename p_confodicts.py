#!/usr/local/bin/python3.4
from decimal import *
from pprint import *

a={'count': {'Newedge': {u'NKD/H15.CM': 30, u'NIY/H15.CM': 455, u'NIY/M15.CM': 1}, 'GH': {u'NKD/H15.CM': 32, u'NIY/H15.CM': 453, u'NIY/M15.CM': 1}}, 'cost': {'Newedge': {u'NKD/H15.CM': Decimal('3330300.00000000'), u'NIY/H15.CM': Decimal('7050017500.00000000'), u'NIY/M15.CM': Decimal('-3987715000.00000000')}, 'GH': {u'NKD/H15.CM': Decimal('3500400.00000000'), u'NIY/H15.CM': Decimal('6879917500.00000000'), u'NIY/M15.CM': Decimal('-3987715000.00000000')}}, 'pnl': {'Newedge': {u'NKD/H15.CM': Decimal('-3330300.00000000'), u'NIY/H15.CM': Decimal('-7050017500.00000000'), u'NIY/M15.CM': Decimal('3987715000.00000000')}, 'GH': {u'NKD/H15.CM': Decimal('-3500400.00000000'), u'NIY/H15.CM': Decimal('-6879917500.00000000'), u'NIY/M15.CM': Decimal('3987715000.00000000')}}, 'qty': {'Newedge': {u'NKD/H15.CM': 39, u'NIY/H15.CM': 815, u'NIY/M15.CM': -478}, 'GH': {u'NKD/H15.CM': 41, u'NIY/H15.CM': 813, u'NIY/M15.CM': -478}}}


# pprint(a)
all_keys = ['qty', 'count']
parties = list(a[all_keys[0]].keys())
syms=list(a[all_keys[0]][parties[0]].keys())
sec_val = lambda all_key_name, s: [a[all_key_name][p][s] for p in parties]
m = lambda x: '{} matched'.format(x[0]) if x[0] == x[1] else '{} vs {}'.format(*x)
headers = [['Security'] + all_keys]
sec_matches = [[s]  + [m(l) for l in [sec_val(k, s) for k in all_keys]] for s in syms]
print('*#'*20)
print(headers)
pprint(sec_matches)
