__author__ = 'apandey'
from base_rw1 import *


from reconwisepy.services.sftp_service import Sftp_Service
from reconwisepy.services.email_service import Email_Service
from reconwisepy.services.service_runner import get_services
from reconwisepy.services.file_parser import FileParser
from reconwisepy.services.settings import *
from reconwisepy.utils import crypts
from reconwisepy.services.settings import _broker_ne, _broker_ms, _broker_abn, _broker_haitong

class StmtInfoLine:
    def __init__(self, line, others=[]):
        [self.__setattr__(*z) for z in line.items()]
        [self.__setattr__(missing_header, '') for missing_header in others]
    def __unicode__(self):
        return '\tLine: {}'.format(str(vars(self)))
    def __repr__(self):
        return '\tLine: {}'.format(str(vars(self)))



def get_pretty_print(item, force=False):
    if force:
        custom_printer = lambda a: (isinstance(a,list) or isinstance(a, tuple)) and \
                                   len(a) > 0 and \
                                   (isinstance(a[0], list) or isinstance(a[0], tuple))
        if custom_printer(item):
            s = ''
            for i in item:
                s += '\n' + str(i)
            return s
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda item: get_pretty_print(item)

def get_encrypted_pwd(pswd):
    key = 'emails_4_service'
    cryp = crypts.AESCipher(key)
    encrypted_pwd = cryp.encrypt(pswd)
    return encrypted_pwd

def get_decrypted_pwd(pswd):
    key = 'emails_4_service'
    cryp = crypts.AESCipher(key)
    decrypted_pwd = cryp.decrypt(pswd)
    return decrypted_pwd

def _print_passwords():
    passwords = [get_encrypted_pwd('r1c3ng35').decode('utf-8') for i in range(1,5)]
    for p in passwords:
        decrypted = get_decrypted_pwd(p)
        print('Password ={}; decrypted={}'.format(p, decrypted))

svcs = get_services()


def upload_contract_relation_file(file_path):
    ftp = ([s for s in svcs if isinstance(s, Sftp_Service)] or [None])[0]
    if ftp:
        print('Trying upload')
        ftp._upload_file_to_v2(file_path)
        print('Upload completed')


def upload_confo_file(file_path):
    broker_name, task_type = _get_broker_name(file_path)
    mail_svc = ([s for s in svcs if isinstance(s, Email_Service)] or [None])[0]
    parser_info = BROKER[broker_name][task_type]
    zip_handler = FileParser(
                        mail_svc.email_settings,
                        file_path,
                        0,
                        parser_info
                    )
    zip_handler._upload_attachment_to_v2()

def _get_broker_name(full_path):
    full_path = full_path.upper()
    is_zip = full_path.endswith('.ZIP')
    if is_zip and _broker_ne.upper() in full_path:
        return _broker_ne, ServiceType.TypeConfo
    if is_zip and _broker_ms.split()[0].upper() in full_path:
        return _broker_ms, ServiceType.TypeConfo
    if is_zip and _broker_abn.upper() in full_path:
        return _broker_abn, ServiceType.TypeConfo
    if not is_zip and 'BBB' in full_path:
        return _broker_ms, ServiceType.TypeRecon
    if not is_zip and 'DAILY STATEMENT' in full_path:
        return _broker_ne, ServiceType.TypeRecon
    if 'HAITONG' in full_path.upper():
        return _broker_haitong, ServiceType.TypeConfo
    print('There has been a problem, we do not know the ServiceType and broker of {}; is_zip={}'.format(full_path, is_zip))



def UPLOAD_FILE(file_path):
    if file_path.lower().endswith('.csv') and 'grasshopper-' in file_path.lower():
        upload_contract_relation_file(file_path)
    else:
        print('filepath={}'.format(file_path))
        upload_confo_file(file_path)

# UPLOAD_FILE(os.path.join('/private/var/reconwise/emails', 'Newedge CME T+1.2014-12-18.zip'))

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Upload files onto V2.')
    parser.add_argument('-f', '--files', nargs='+',
                       help='list of files to convert', required=True)
    args = parser.parse_args()
    files = args.files
    for f in files:
        UPLOAD_FILE(f)


