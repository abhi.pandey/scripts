__author__ = 'apandey'
#!/usr/local/bin/python3.4
import os
from p_rw1_upload_file import UPLOAD_FILE

all_files = '''
/home/ec2-user/data/v1/emails/Newedge CME T+1.2015-05-22.zip
/home/ec2-user/data/v1/emails/Daily Statements 2015-05-22.xls
/home/ec2-user/data/v1/emails/Newedge US Non-CME T+1.2015-05-25.zip
/home/ec2-user/data/v1/emails/Newedge SGX T+1.2015-05-25.zip
/home/ec2-user/data/v1/emails/Newedge OSE T+1.2015-05-25.zip
/home/ec2-user/data/v1/sftp/grasshopper-contract_size_info-20150526_060010.csv
/home/ec2-user/data/v1/sftp/grasshopper-delta_info-20150526_060204.csv
/home/ec2-user/data/v1/sftp/grasshopper-fair_value-20150526_060308.csv
/home/ec2-user/data/v1/emails/Newedge US Non-CME T+1.2015-05-25.zip
/home/ec2-user/data/v1/emails/Daily Statements 2015-05-25.xls
/home/ec2-user/data/v1/emails/GRASHOPP0.RTM102LNXNH.6244256.20150525.203323.4758.00000028F74A0AC317BE6355A04A0000.txt
/home/ec2-user/data/v1/emails/BBB02980367.MAC001X.6244255.20150525.004452.5509.000000DFF74A0AC3A0FA6355CC250000.txt
/home/ec2-user/data/v1/emails/Morgan Stanley TSE Stocks.2015-05-26.zip
/home/ec2-user/data/v1/emails/Morgan Stanley Sgx futures.2015-05-26.zip
/home/ec2-user/data/v1/emails/Morgan Stanley Japan futures.2015-05-26.zip'''

folder = '/home/ec2-user/data/v1/emails/'

all_files1 = '''
/home/ec2-user/data/v1/emails/Newedge OSE T+1.2015-05-25.zip
'''

files = [f for f in all_files1.split('\n') if f and 'GRASHOPP' not in f]
for f in files:
    # print('file={}'.format(f))
    just_filename = f.replace(folder, '')
    if just_filename.startswith('Morgan Stanley') or just_filename.startswith('Newedge '):
        base = os.path.basename(f)
        splits = os.path.splitext(f)
        f = os.path.join(splits[0], base)
        # print('base = {}; splits={}'.format(base, splits))
        # print(' new file name = {}. exists? = {} '.format(new_p, os.path.exists(new_p)))

    print('file = {}; found?={}'.format(f, os.path.exists(f)))
    if os.path.exists(f):
        UPLOAD_FILE(f)

