#!/usr/local/bin/python3.4

import json

def get_file_text(file_path):
    with open(file_path, 'rt') as file_placeholder:
        lines = file_placeholder.readlines()
        text = ''.join(lines)
        return text

def save_json(file_path, dict_data):
    with open(file_path, 'w') as outfile:
        json.dump(dict_data, outfile, indent=4, separators=(',', ':'), sort_keys=True)


def read_json_text(file_path):
    file_text = get_file_text(file_path)
    #json_acceptable_string = file_text.replace("'", "\"")
    json_acceptable_string = file_text
    json_text = json.loads(json_acceptable_string)
    return json_text

files = ['Reconcile__NewEdge_2014-12-17_871.json',
                'Reconcile__Morgan_Stanley_2014-12-17_875.json']

for file in files:
    ddata = read_json_text(file)
    save_json('OUT_' + file, ddata)

