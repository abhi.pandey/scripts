__author__ = 'apandey'

from django.db.models import Sum, Avg
import django, os, sys
from django.db.models import F, FloatField
from collections import defaultdict
import pprint
from itertools import chain, groupby
import logging


rw_path1 = '/Users/apandey/code/tilde/reconwisev1/src/'
os.chdir(rw_path1)

sys.path.append(rw_path1)
sys.path.append(os.path.join(rw_path1, 'main/python'))
print('path={}'.format(os.getcwd()))


from reconwisepy.services.sftp_service import Sftp_Service
from reconwisepy.services.email_service import Email_Service
from reconwisepy.services.service_runner import get_services
from reconwisepy.services.file_parser import FileParser
from reconwisepy.services.settings import *
from reconwisepy.utils import crypts
from reconwisepy.services.settings import _broker_ne, _broker_ms, _broker_abn, _broker_haitong
from reconwisepy.utils import utilities

def get_pretty_print(item, force=False):
    if force:
        custom_printer = lambda a: (isinstance(a,list) or isinstance(a, tuple)) and \
                                   len(a) > 0 and \
                                   (isinstance(a[0], list) or isinstance(a[0], tuple))
        if custom_printer(item):
            s = ''
            for i in item:
                s += '\n' + str(i)
            return s
    pp = pprint.PrettyPrinter(indent=4)
    return pp.pformat(item)


pp = lambda item: get_pretty_print(item)

from reconwisepy.utils.utilities import *
from reconwisepy.db.db_layer import MyData
from reconwisepy.utils.utilities import *
from reconwisepy.services.service_runner import *
